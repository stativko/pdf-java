package iggerman.by;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.AcroFields.FieldPosition;
import com.itextpdf.text.pdf.AcroFields.Item;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import jsimple.json.objectmodel.JsonArray;
import jsimple.json.objectmodel.JsonObject;

public class Main
{
    public static void main(String[] args)
            throws IOException, DocumentException
    {
        String pdfFile = args[0];

        PdfReader reader = new PdfReader(pdfFile);
        AcroFields fields = reader.getAcroFields();

        Map<String, AcroFields.Item> fieldsMap = fields.getFields();
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(args[1]));

        int i = 1;
        for (Map.Entry fieldItem : fieldsMap.entrySet())
        {
            String name = (String)fieldItem.getKey();
            List<AcroFields.FieldPosition> positions = fields.getFieldPositions(name);

            Rectangle rect = ((AcroFields.FieldPosition)positions.get(0)).position;

            int page = ((AcroFields.FieldPosition)positions.get(0)).page;

            PdfContentByte content = stamper.getOverContent(page);

            String text = Integer.toString(i);

            BaseColor color = new BaseColor(68, 171, 210);
            BaseFont bf = BaseFont.createFont();
            Phrase phrase = new Phrase(text, new Font(bf, 8.0F, 0, color));
            ColumnText.showTextAligned(content, 1, phrase, (rect.getLeft() + rect.getRight()) / 2.0F, rect.getBottom() - bf.getDescentPoint(text, 8.0F) + 3.0F, 0.0F);

            content.saveState();

            rect.setBorder(15);
            rect.setBorderWidth(1.0F);
            rect.setBorderColor(color);

            content.rectangle(rect);
            content.stroke();
            content.restoreState();

            JsonObject obj = new JsonObject();
            String value = fields.getField(name);
            String type = resolveFieldTypeText(fields.getFieldType(name));

            obj.add("field_id", Integer.valueOf(i));
            obj.add("field_name", name);
            obj.add("field_value", value);
            obj.add("field_type", type);
            obj.add("field_rect", rectangleToJsonArray(rect));

            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(System.out));

            out.write(obj.toString() + "|~|");
            out.flush();

            i++;
        }
        stamper.close();
        reader.close();
    }

    private static String resolveFieldTypeText(int type)
    {
        String text = "Unknown";
        switch (type)
        {
            case 4:
                text = "TEXT";
                break;
            case 2:
                text = "CHECKBOX";
                break;
            case 5:
                text = "LIST";
                break;
            case 1:
                text = "PUSH_BUTTON";
                break;
            case 3:
                text = "RADIO_BUTTON";
        }
        return text;
    }

    private static JsonArray rectangleToJsonArray(Rectangle rectangle)
    {
        JsonArray arr = new JsonArray();
        arr.add(Float.valueOf(rectangle.getLeft()));
        arr.add(Float.valueOf(rectangle.getBottom()));
        arr.add(Float.valueOf(rectangle.getRight()));
        arr.add(Float.valueOf(rectangle.getTop()));
        return arr;
    }
}
